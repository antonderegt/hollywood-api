﻿using AutoMapper;
using HollywoodAPI.Models;

namespace HollywoodAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            // Franchise <-> FranchiseReadDTO
            CreateMap<Franchise, FranchiseReadDTO>()
                .ReverseMap();

            // Franchise <-> FranchiseCreateDTO
            CreateMap<Franchise, FranchiseCreateDTO>()
                .ReverseMap();

            // Franchise <-> FranchiseEditDTO
            CreateMap<Franchise, FranchiseEditDTO>().ReverseMap();
        }

    }
}
