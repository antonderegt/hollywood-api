﻿using AutoMapper;
using HollywoodAPI.Models;

namespace HollywoodAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            // Movie <-> MovieReadDTO
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(m => m.FranchiseId))
                .ReverseMap();

            // Movie <-> MovieCreateDTO
            CreateMap<Movie, MovieCreateDTO>()
                .ForMember(mdto => mdto.FranchiseId, opt => opt
                .MapFrom(m => m.FranchiseId))
                .ReverseMap();

            // Movie <-> MovieEditDTO
            CreateMap<Movie, MovieEditDTO>()
                .ForMember(mdto => mdto.FranchiseId, opt => opt
                .MapFrom(m => m.FranchiseId))
                .ReverseMap();
        }
    }
}
