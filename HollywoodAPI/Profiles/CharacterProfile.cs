﻿using AutoMapper;
using HollywoodAPI.Models;

namespace HollywoodAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            // Character <-> CharacterReadDTO
            CreateMap<Character, CharacterReadDTO>().ReverseMap();

            // Character <-> CharacterEditDTO
            CreateMap<Character, CharacterEditDTO>().ReverseMap();

            // Character <-> CharacterCreateDTO
            CreateMap<Character, CharacterCreateDTO>().ReverseMap();
        }

    }
}
