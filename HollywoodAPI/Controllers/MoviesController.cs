﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HollywoodAPI.Models;
using System.Net.Mime;
using AutoMapper;

namespace HollywoodAPI.Controllers
{
    [Route("api/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly HollywoodDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(HollywoodDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all movies from database.
        /// </summary>
        /// <returns>List of movies</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovie()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _context.Movie.ToListAsync());
        }

        /// <summary>
        /// Get specific movie by Id.
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns>Movie on success, on fail it returns 404NotFound</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _context.Movie.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Get all characters from a movie, by movie Id.
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns>List of characters on success, on fail it returns 404NotFound</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInMovie(int id)
        {
            var movie = await _context.Movie.Include(m => m.Characters).Where(m => m.MovieId == id).FirstAsync();

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterReadDTO>>(movie.Characters.ToList());
        }

        /// <summary>
        /// Edit movie by Id.
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <param name="movie">Updated movie</param>
        /// <returns>204NoContent on success, on fail it returns 400BadRequest or 404NotFound</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movie)
        {
            if (id != movie.MovieId)
            {
                return BadRequest();
            }

            Movie domainMovie = _mapper.Map<Movie>(movie);
            _context.Entry(domainMovie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add new move to database.
        /// </summary>
        /// <param name="movie">New movie</param>
        /// <returns>201Created</returns>
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie(MovieCreateDTO cdto)
        {
            Movie movie = _mapper.Map<Movie>(cdto);
            _context.Movie.Add(movie);
            await _context.SaveChangesAsync();

            MovieReadDTO rdto = _mapper.Map<MovieReadDTO>(movie);
            return CreatedAtAction("GetMovie", new { id = rdto.MovieId }, rdto);
        }

        /// <summary>
        /// Add character to movie by id.
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <param name="characterId">Character Id</param>
        /// <returns>201Created on success, on fails it returns 404NotFound</returns>
        [HttpPut("{id}/add-character")]
        public async Task<ActionResult<MovieReadDTO>> AddCharacterToMovie(int id, int characterId)
        {
            Character character = await _context.Character.FindAsync(characterId);
            Movie movie = await _context.Movie.Include(m => m.Characters).Where(m => m.MovieId == id).FirstAsync();

            if (movie == null || character == null)
            {
                return NotFound();
            }

            movie.Characters.Add(character);

            await _context.SaveChangesAsync();

            MovieReadDTO rdto = _mapper.Map<MovieReadDTO>(movie);
            return CreatedAtAction("GetMovie", new { id = rdto.MovieId }, rdto);
        }

        /// <summary>
        /// Add movie to existing franchise, by Id.
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <param name="franchiseId">Franchise Id</param>
        /// <returns>201Created on success, on fail it returns 404NotFound</returns>
        [HttpPut("{id}/add-franchise")]
        public async Task<ActionResult<MovieReadDTO>> AddFranchiseToMovie(int id, int franchiseId)
        {
            Movie movie = await _context.Movie.FindAsync(id);
            Franchise franchise = await _context.Franchise.FindAsync(franchiseId);

            if (movie == null || franchise == null)
            {
                return NotFound();
            }

            movie.FranchiseId = franchiseId;

            await _context.SaveChangesAsync();

            MovieReadDTO rdto = _mapper.Map<MovieReadDTO>(movie);
            return CreatedAtAction("GetMovie", new { id = rdto.MovieId }, rdto);
        }

        /// <summary>
        /// Remove movie from franchise by Id.
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns>201Created on success, on fail it returns 404NotFound</returns>
        [HttpPut("{id}/remove-franchise")]
        public async Task<ActionResult<MovieReadDTO>> RemoveFranchiseFromMovie(int id)
        {
            Movie movie = await _context.Movie.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            movie.FranchiseId = null;

            await _context.SaveChangesAsync();

            MovieReadDTO rdto = _mapper.Map<MovieReadDTO>(movie);
            return CreatedAtAction("GetMovie", new { id = rdto.MovieId }, rdto);
        }

        /// <summary>
        /// Delete movie by Id.
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <returns>204NoContent on success, on fail it returns 404NotFound</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movie.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movie.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Delete character from movie by Id.
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <param name="characterId">Character Id</param>
        /// <returns>204NoContent on success, on fail it returns 404NotFound</returns>
        [HttpDelete("{id}/delete-character")]
        public async Task<IActionResult> DeleteCharacterFromMovie(int id, int characterId)
        {
            Character character = await _context.Character.FindAsync(characterId);
            Movie movie = await _context.Movie.Include(m => m.Characters).Where(m => m.MovieId == id).FirstAsync();

            if (movie == null || character == null)
            {
                return NotFound();
            }

            movie.Characters.Remove(character);

            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool MovieExists(int id)
        {
            return _context.Movie.Any(e => e.MovieId == id);
        }
    }
}
