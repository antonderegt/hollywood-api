﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HollywoodAPI.Models;
using AutoMapper;
using System.Net.Mime;

namespace HollywoodAPI.Controllers
{
    [Route("api/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly HollywoodDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(HollywoodDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all franchises.
        /// </summary>
        /// <returns>List of franchises</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchise()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchise.ToListAsync());
        }

        /// <summary>
        /// Get specific franchise by Id.
        /// </summary>
        /// <param name="id">Franchises Id</param>
        /// <returns>Franchise on success, on fail it returns 404NotFound</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _context.Franchise.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Get movies from specific franchise, by franchise Id.
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>List of movies on success, on fail it returns 404NotFound</returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesInFranchise(int id)
        {
            var franchise = await _context.Franchise.Include(f => f.Movies).Where(f => f.FranchiseId == id).FirstAsync();

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<MovieReadDTO>>(franchise.Movies.ToList());
        }

        /// <summary>
        /// Get all characters in a franchise by franchise Id.
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>List of characters on success, on fail it returns 404NotFound</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInFranchise(int id)
        {
            var franchise = await _context.Franchise.Include(f => f.Movies).ThenInclude(m => m.Characters).Where(f => f.FranchiseId == id).FirstAsync();

            if (franchise == null)
            {
                return NotFound();
            }

            var characterList = new HashSet<CharacterReadDTO>();

            foreach (var movie in franchise.Movies)
            {
                foreach (var character in movie.Characters)
                {
                    CharacterReadDTO cdto = _mapper.Map<CharacterReadDTO>(character);
                    characterList.Add(cdto);
                }
            }

            return characterList;
        }

        /// <summary>
        /// Edit franchise by Id.
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <param name="franchise">Updated franchise</param>
        /// <returns>204NoContent on success, on fail it returns 400BadRequest or 404NotFound</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchise)
        {
            if (id != franchise.FranchiseId)
            {
                return BadRequest();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);
            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add franchise to database.
        /// </summary>
        /// <param name="franchise">New franchise</param>
        /// <returns>201Created on success</returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO cdto)
        {
            Franchise franchise = _mapper.Map<Franchise>(cdto);
            _context.Franchise.Add(franchise);
            await _context.SaveChangesAsync();

            FranchiseReadDTO rdto = _mapper.Map<FranchiseReadDTO>(franchise);
            return CreatedAtAction("GetFranchise", new { id = rdto.FranchiseId }, rdto);
        }

        /// <summary>
        /// Delete franchise by Id.
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>204NoContent on success, on fail it returns 404NotFound</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchise.Include(f => f.Movies).Where(f => f.FranchiseId == id).FirstAsync();
            if (franchise == null)
            {
                return NotFound();
            }

            franchise.Movies.Select(m => m.FranchiseId = null);

            _context.Franchise.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchise.Any(e => e.FranchiseId == id);
        }
    }
}
