﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HollywoodAPI.Models;
using AutoMapper;
using System.Net.Mime;

namespace HollywoodAPI.Controllers
{
    [Route("api/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly HollywoodDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(HollywoodDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all characters from database.
        /// </summary>
        /// <returns>List of characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacter()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _context.Character.ToListAsync());
        }

        /// <summary>
        /// Get specific character by Id.
        /// </summary>
        /// <param name="id">Character Id</param>
        /// <returns>Character on success, on fail it returns 404NotFound</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _context.Character.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Edit character by Id
        /// </summary>
        /// <param name="id">Character Id</param>
        /// <param name="character">Updated character</param>
        /// <returns>204NoContent on success, on fail it returns 400BadRequest or 404NotFound</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO character)
        {
            if (id != character.CharacterId)
            {
                return BadRequest();
            }

            Character domainCharacter = _mapper.Map<Character>(character);
            _context.Entry(domainCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add new character to database.
        /// </summary>
        /// <param name="character">New character</param>
        /// <returns>201Created character</returns>
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter(CharacterCreateDTO character)
        {
            Character domainCharacter = _mapper.Map<Character>(character);
            _context.Character.Add(domainCharacter);
            await _context.SaveChangesAsync();

            CharacterReadDTO cdto = _mapper.Map<CharacterReadDTO>(domainCharacter);
            return CreatedAtAction("GetCharacter", new { id = cdto.CharacterId }, cdto);
        }

        /// <summary>
        /// Delete character from database.
        /// </summary>
        /// <param name="id">Character Id</param>
        /// <returns>204NoContent on success, on fail it returns 404NotFound</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _context.Character.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Character.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return _context.Character.Any(e => e.CharacterId == id);
        }
    }
}
