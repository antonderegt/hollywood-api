﻿using Microsoft.EntityFrameworkCore;

namespace HollywoodAPI.Models
{
    public class HollywoodDbContext : DbContext
    {
        public HollywoodDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Character> Character { get; set; }
        public DbSet<Movie> Movie { get; set; }
        public DbSet<Franchise> Franchise { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region SeedingMovies

            // Create movies
            var movieTenet = new Movie
            {
                MovieId = 1,
                Title = "Tenet",
                Genre = "Mindfuck",
                ReleaseYear = 2020,
                Director = "Christopher Nolan",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FTenet_(film)&psig=AOvVaw26RK3wvNTNyIuV4M-Nj_hD&ust=1622208349931000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPC0uZb76fACFQAAAAAdAAAAABAD",
                TrailerUrl = "https://www.youtube.com/watch?v=LdOM0x0XDMo",
                FranchiseId = 1
            };

            var movieBatmanBegins = new Movie
            {
                MovieId = 2,
                Title = "Batman Begins",
                Genre = "Actie",
                ReleaseYear = 2005,
                Director = "Christopher Nolan",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.imdb.com%2Ftitle%2Ftt0372784%2F&psig=AOvVaw27d_zLasqUXnWE4aBfDK_P&ust=1622208531646000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCJCzue376fACFQAAAAAdAAAAABAD",
                TrailerUrl = "https://www.youtube.com/watch?v=neY2xVmOfUM",
                FranchiseId = 1
            };

            var movieLAC = new Movie
            {
                MovieId = 3,
                Title = "Law Abiding Citizen",
                Genre = "Actie",
                ReleaseYear = 2009,
                Director = "F. Gary Gray",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FLaw_Abiding_Citizen&psig=AOvVaw2qVjvxj9aOJ0VfuvjLXSgR&ust=1622208629003000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNiQgJ386fACFQAAAAAdAAAAABAD",
                TrailerUrl = "https://www.youtube.com/watch?v=LX6kVRsdXW4",
                FranchiseId = 2
            };

            var movieMatrix = new Movie
            {
                MovieId = 4,
                Title = "Matrix",
                Genre = "Actie",
                ReleaseYear = 1999,
                Director = "Lana Wachowski",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FThe_Matrix&psig=AOvVaw0diVC-tG_ouSFIGf7bNnrx&ust=1622208709730000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCMCguMH86fACFQAAAAAdAAAAABAD",
                TrailerUrl = "https://www.youtube.com/watch?v=vKQi3bBA1y8",
                FranchiseId = 1
            };

            var movieLotR = new Movie
            {
                MovieId = 5,
                Title = "Lord Of The Rings",
                Genre = "Fantasy",
                ReleaseYear = 2003,
                Director = "Peter Jackson",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.superguide.nl%2Fnieuws%2Fwaarom-haat-de-familie-tolkien-de-lotr-films-films&psig=AOvVaw3Xdt2bupL6hHNPFY7mZ8W-&ust=1622215332500000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCIi6mJeV6vACFQAAAAAdAAAAABAJ",
                TrailerUrl = "https://www.youtube.com/watch?v=V75dMMIW2B4",
                FranchiseId = 3
            };

            // Add movies to database
            modelBuilder.Entity<Movie>()
                .HasData(movieTenet, movieBatmanBegins, movieLAC, movieMatrix, movieLotR);

            #endregion

            #region SeedingCharacters

            // Create characters
            var characterTenet1 = new Character
            {
                CharacterId = 1,
                Name = "John David Washington",
                Alias = "The Protagonist",
                Gender = "Male",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.refinery29.com%2Fen-us%2F2020%2F12%2F10220396%2Ftenet-movie-cast-characters-actors&psig=AOvVaw3wrBWD2wg-MmQl7fBmX1pW&ust=1622208926280000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCJCVgKr96fACFQAAAAAdAAAAABAD"
            };

            var characterTenet2 = new Character
            {
                CharacterId = 2,
                Name = "Robert Pattinson",
                Alias = "Neil",
                Gender = "Male",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.pinterest.at%2Fpin%2F628392954251531249%2F&psig=AOvVaw1qqT2r2KLKFuLNfu7Z70eV&ust=1622209028451000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLDn2dr96fACFQAAAAAdAAAAABAD"
            };

            var characterBB1 = new Character
            {
                CharacterId = 3,
                Name = "Christian Bale",
                Alias = "Bruce",
                Gender = "Male",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fchrisnolan.fandom.com%2Fwiki%2FBruce_Wayne&psig=AOvVaw1ta2VcT33FJXsNTVvDGssa&ust=1622209098720000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNjYv_r96fACFQAAAAAdAAAAABAT"
            };

            var characterBB2 = new Character
            {
                CharacterId = 4,
                Name = "Heath Ledger",
                Alias = "The Joker",
                Gender = "Male",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FJoker_(The_Dark_Knight)&psig=AOvVaw1SHGHsIHP6yXRux26FUSMB&ust=1622209164935000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNCMlZr-6fACFQAAAAAdAAAAABAD"
            };

            var characterLAC1 = new Character
            {
                CharacterId = 5,
                Name = "Gerard Butler",
                Alias = "Clyde Shelton",
                Gender = "Male",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.looper.com%2F354652%2Fthe-ending-of-law-abiding-citizen-explained%2F&psig=AOvVaw1GJyNbqWiAz-JNuLj-r1Ee&ust=1622209224166000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLDnv7b-6fACFQAAAAAdAAAAABAD"
            };

            var characterLAC2 = new Character
            {
                CharacterId = 6,
                Name = "Jamie Foxx",
                Alias = "Nick Rice",
                Gender = "Male",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.imdb.com%2Fname%2Fnm0004937%2F&psig=AOvVaw321qd7tb76RWfJM62unWV5&ust=1622209259291000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCKjsp8f-6fACFQAAAAAdAAAAABAD"
            };

            var characterM1 = new Character
            {
                CharacterId = 7,
                Name = "Keanu Reeves",
                Alias = "Neo",
                Gender = "Male",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.manners.nl%2Fthe-matrix-4-beelden-neo-nieuwe-look%2F&psig=AOvVaw287b2pDQhrgQfeAloUthRg&ust=1622209320699000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNje5-n-6fACFQAAAAAdAAAAABAD"
            };

            var characterM2 = new Character
            {
                CharacterId = 8,
                Name = "Carrie Anne Moss",
                Alias = "Trinity",
                Gender = "Female",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.pinterest.com%2Fnoeroe2324%2Fcarrie-ann-moss%2F&psig=AOvVaw01IWIgYJy6WWtLgfz63gry&ust=1622209366374000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCODO8_n-6fACFQAAAAAdAAAAABAE"
            };

            var characterLotR1 = new Character
            {
                CharacterId = 9,
                Name = "Liv Tyler",
                Alias = "Arwen Undómiel",
                Gender = "Female",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Flotr.fandom.com%2Fwiki%2FArwen&psig=AOvVaw2ZALhMa9pomnGIoyDfhalg&ust=1622215504631000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCOD7ueyV6vACFQAAAAAdAAAAABAO"
            };

            var characterLotR2 = new Character
            {
                CharacterId = 10,
                Name = "Elijah Wood",
                Alias = "Frodo",
                Gender = "Male",
                PictureUrl = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.nu.nl%2Fachterklap%2F3352906%2Felijah-wood-ging-moeilijke-tijd.html&psig=AOvVaw1C8CF8y6ZgcpZYIWksVert&ust=1622215455266000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCMCY99GV6vACFQAAAAAdAAAAABAD"
            };

            // Add characters to database
            modelBuilder.Entity<Character>()
                .HasData(characterTenet1, characterTenet2, characterBB1, characterBB2, characterLAC1, characterLAC2, characterM1, characterM2, characterLotR1, characterLotR2);

            #endregion

            #region SeedingFranchises

            // Create franchises
            var franchise1 = new Franchise
            {
                FranchiseId = 1,
                Name = "Warner Bros. Pictures",
                Description = "Warner Bros. Entertainment Inc. is an American diversified multinational mass media and entertainment conglomerate headquartered at the Warner Bros. Studios complex in Burbank, California, and a subsidiary of AT&T's WarnerMedia through its Studios & Networks division."
            };

            var franchise2 = new Franchise
            {
                FranchiseId = 2,
                Name = "Overture Films",
                Description = "Overture Films was an American film production and distribution company and a subsidiary of Starz (then subsidiary of Liberty Media)."
            };

            var franchise3 = new Franchise
            {
                FranchiseId = 3,
                Name = "New Line Cinema",
                Description = "New Line Productions, Inc., doing business as New Line Cinema, is an American film production studio and a label of the Warner Bros. Pictures Group division of Warner Bros. Entertainment."
            };

            // Add franchises to database
            modelBuilder.Entity<Franchise>()
                .HasData(franchise1, franchise2, franchise3);

            #endregion

            #region SeedingCharacterMovie

            // Seed linking table
            modelBuilder
                .Entity<Character>()
                .HasMany(c => c.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity(j => j.HasData(
                            new { MoviesMovieId = 1, CharactersCharacterId = 1 },
                            new { MoviesMovieId = 1, CharactersCharacterId = 2 },
                            new { MoviesMovieId = 2, CharactersCharacterId = 3 },
                            new { MoviesMovieId = 2, CharactersCharacterId = 4 },
                            new { MoviesMovieId = 3, CharactersCharacterId = 5 },
                            new { MoviesMovieId = 3, CharactersCharacterId = 6 },
                            new { MoviesMovieId = 4, CharactersCharacterId = 7 },
                            new { MoviesMovieId = 4, CharactersCharacterId = 8 },
                            new { MoviesMovieId = 5, CharactersCharacterId = 9 },
                            new { MoviesMovieId = 5, CharactersCharacterId = 10 }
                        ));

            #endregion
        }
    }
}
