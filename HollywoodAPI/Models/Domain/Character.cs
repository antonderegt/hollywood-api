﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HollywoodAPI.Models
{
    public class Character
    {
        public int CharacterId { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [Required]
        [MaxLength(20)]
        public string Gender { get; set; }
        [Required]
        public string PictureUrl { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
