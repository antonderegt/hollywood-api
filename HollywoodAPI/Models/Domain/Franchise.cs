﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HollywoodAPI.Models
{
    public class Franchise
    {
        public int FranchiseId { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
