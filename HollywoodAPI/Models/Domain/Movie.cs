﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HollywoodAPI.Models
{
    public class Movie
    {
        public int MovieId { get; set; }
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        [Required]
        [MaxLength(50)]
        public string Genre { get; set; }
        [Required]
        public int ReleaseYear { get; set; }
        [Required]
        [MaxLength(50)]
        public string Director { get; set; }
        [Required]
        public string PictureUrl { get; set; }
        [Required]
        public string TrailerUrl { get; set; }

        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        public ICollection<Character> Characters { get; set; }
    }
}
