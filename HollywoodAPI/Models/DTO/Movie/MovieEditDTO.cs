﻿namespace HollywoodAPI.Models
{
    public class MovieEditDTO
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string PictureUrl { get; set; }
        public string TrailerUrl { get; set; }
        public int? FranchiseId { get; set; }
    }
}
