﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HollywoodAPI.Migrations
{
    public partial class InitialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Character",
                columns: table => new
                {
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    PictureUrl = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Character", x => x.CharacterId);
                });

            migrationBuilder.CreateTable(
                name: "Franchise",
                columns: table => new
                {
                    FranchiseId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchise", x => x.FranchiseId);
                });

            migrationBuilder.CreateTable(
                name: "Movie",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    ReleaseYear = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    PictureUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TrailerUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FranchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie", x => x.MovieId);
                    table.ForeignKey(
                        name: "FK_Movie_Franchise_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchise",
                        principalColumn: "FranchiseId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CharacterMovie",
                columns: table => new
                {
                    CharactersCharacterId = table.Column<int>(type: "int", nullable: false),
                    MoviesMovieId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMovie", x => new { x.CharactersCharacterId, x.MoviesMovieId });
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Character_CharactersCharacterId",
                        column: x => x.CharactersCharacterId,
                        principalTable: "Character",
                        principalColumn: "CharacterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Movie_MoviesMovieId",
                        column: x => x.MoviesMovieId,
                        principalTable: "Movie",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "CharacterId", "Alias", "Gender", "Name", "PictureUrl" },
                values: new object[,]
                {
                    { 1, "The Protagonist", "Male", "John David Washington", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.refinery29.com%2Fen-us%2F2020%2F12%2F10220396%2Ftenet-movie-cast-characters-actors&psig=AOvVaw3wrBWD2wg-MmQl7fBmX1pW&ust=1622208926280000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCJCVgKr96fACFQAAAAAdAAAAABAD" },
                    { 2, "Neil", "Male", "Robert Pattinson", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.pinterest.at%2Fpin%2F628392954251531249%2F&psig=AOvVaw1qqT2r2KLKFuLNfu7Z70eV&ust=1622209028451000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLDn2dr96fACFQAAAAAdAAAAABAD" },
                    { 3, "Bruce", "Male", "Christian Bale", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fchrisnolan.fandom.com%2Fwiki%2FBruce_Wayne&psig=AOvVaw1ta2VcT33FJXsNTVvDGssa&ust=1622209098720000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNjYv_r96fACFQAAAAAdAAAAABAT" },
                    { 4, "The Joker", "Male", "Heath Ledger", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FJoker_(The_Dark_Knight)&psig=AOvVaw1SHGHsIHP6yXRux26FUSMB&ust=1622209164935000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNCMlZr-6fACFQAAAAAdAAAAABAD" },
                    { 5, "Clyde Shelton", "Male", "Gerard Butler", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.looper.com%2F354652%2Fthe-ending-of-law-abiding-citizen-explained%2F&psig=AOvVaw1GJyNbqWiAz-JNuLj-r1Ee&ust=1622209224166000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLDnv7b-6fACFQAAAAAdAAAAABAD" },
                    { 6, "Nick Rice", "Male", "Jamie Foxx", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.imdb.com%2Fname%2Fnm0004937%2F&psig=AOvVaw321qd7tb76RWfJM62unWV5&ust=1622209259291000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCKjsp8f-6fACFQAAAAAdAAAAABAD" },
                    { 7, "Neo", "Male", "Keanu Reeves", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.manners.nl%2Fthe-matrix-4-beelden-neo-nieuwe-look%2F&psig=AOvVaw287b2pDQhrgQfeAloUthRg&ust=1622209320699000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNje5-n-6fACFQAAAAAdAAAAABAD" },
                    { 8, "Trinity", "Female", "Carrie Anne Moss", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.pinterest.com%2Fnoeroe2324%2Fcarrie-ann-moss%2F&psig=AOvVaw01IWIgYJy6WWtLgfz63gry&ust=1622209366374000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCODO8_n-6fACFQAAAAAdAAAAABAE" },
                    { 9, "Arwen Undómiel", "Female", "Liv Tyler", "https://www.google.com/url?sa=i&url=https%3A%2F%2Flotr.fandom.com%2Fwiki%2FArwen&psig=AOvVaw2ZALhMa9pomnGIoyDfhalg&ust=1622215504631000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCOD7ueyV6vACFQAAAAAdAAAAABAO" },
                    { 10, "Frodo", "Male", "Elijah Wood", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.nu.nl%2Fachterklap%2F3352906%2Felijah-wood-ging-moeilijke-tijd.html&psig=AOvVaw1C8CF8y6ZgcpZYIWksVert&ust=1622215455266000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCMCY99GV6vACFQAAAAAdAAAAABAD" }
                });

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "FranchiseId", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Warner Bros. Entertainment Inc. is an American diversified multinational mass media and entertainment conglomerate headquartered at the Warner Bros. Studios complex in Burbank, California, and a subsidiary of AT&T's WarnerMedia through its Studios & Networks division.", "Warner Bros. Pictures" },
                    { 2, "Overture Films was an American film production and distribution company and a subsidiary of Starz (then subsidiary of Liberty Media).", "Overture Films" },
                    { 3, "New Line Productions, Inc., doing business as New Line Cinema, is an American film production studio and a label of the Warner Bros. Pictures Group division of Warner Bros. Entertainment.", "New Line Cinema" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "MovieId", "Director", "FranchiseId", "Genre", "PictureUrl", "ReleaseYear", "Title", "TrailerUrl" },
                values: new object[,]
                {
                    { 1, "Christopher Nolan", 1, "Mindfuck", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FTenet_(film)&psig=AOvVaw26RK3wvNTNyIuV4M-Nj_hD&ust=1622208349931000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPC0uZb76fACFQAAAAAdAAAAABAD", 2020, "Tenet", "https://www.youtube.com/watch?v=LdOM0x0XDMo" },
                    { 2, "Christopher Nolan", 1, "Actie", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.imdb.com%2Ftitle%2Ftt0372784%2F&psig=AOvVaw27d_zLasqUXnWE4aBfDK_P&ust=1622208531646000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCJCzue376fACFQAAAAAdAAAAABAD", 2005, "Batman Begins", "https://www.youtube.com/watch?v=neY2xVmOfUM" },
                    { 4, "Lana Wachowski", 1, "Actie", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FThe_Matrix&psig=AOvVaw0diVC-tG_ouSFIGf7bNnrx&ust=1622208709730000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCMCguMH86fACFQAAAAAdAAAAABAD", 1999, "Matrix", "https://www.youtube.com/watch?v=vKQi3bBA1y8" },
                    { 3, "F. Gary Gray", 2, "Actie", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FLaw_Abiding_Citizen&psig=AOvVaw2qVjvxj9aOJ0VfuvjLXSgR&ust=1622208629003000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNiQgJ386fACFQAAAAAdAAAAABAD", 2009, "Law Abiding Citizen", "https://www.youtube.com/watch?v=LX6kVRsdXW4" },
                    { 5, "Peter Jackson", 3, "Fantasy", "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.superguide.nl%2Fnieuws%2Fwaarom-haat-de-familie-tolkien-de-lotr-films-films&psig=AOvVaw3Xdt2bupL6hHNPFY7mZ8W-&ust=1622215332500000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCIi6mJeV6vACFQAAAAAdAAAAABAJ", 2003, "Lord Of The Rings", "https://www.youtube.com/watch?v=V75dMMIW2B4" }
                });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharactersCharacterId", "MoviesMovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 2 },
                    { 4, 2 },
                    { 7, 4 },
                    { 8, 4 },
                    { 5, 3 },
                    { 6, 3 },
                    { 9, 5 },
                    { 10, 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_MoviesMovieId",
                table: "CharacterMovie",
                column: "MoviesMovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Movie_FranchiseId",
                table: "Movie",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterMovie");

            migrationBuilder.DropTable(
                name: "Character");

            migrationBuilder.DropTable(
                name: "Movie");

            migrationBuilder.DropTable(
                name: "Franchise");
        }
    }
}
