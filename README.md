**Week 7 assignment**

For the .NET Full Stack Noroff course we had to create a ASP.NET Core Web API and document it. This Web API is about movies, characters and franchises. 

This assignment includes the following:
- Database made in SQL Server through EF Core with a RESTful API
- WEB API using ASP.NET Core


**Appendix A**

Business rules for assignment:
- One movie contains many characters, and a character can play in multiple movies.
-  One movie belongs to one franchise, but a franchise can contain many movies.

Furthermore the database has to be filled with dummy data usng seeding data. (at least 3 movies, with some characters and franchises)


**Appendix B**

API Requirements:

_Updating related data_
- Add a character to an existing movie
- Remove a character from an existing movie
- Add a movie to an existing franchise 
- Remove a movie from an existing franchise

_Reporting_
- Get all movies in a franchise
- Get all the characters in a movie
- Get all the characters in a franchise 

Data transfer objects (DTOs) were created for all the models clients interact with. AutoMapper was used to create mappings between the domain models (entities) and the DTOs.

Lastly documentation was added for Swagger. 
